# GraphLabs

## Как запустить

1 Скачать репозиторий  
git clone https://gitlab.com/WorldOverHeaven/graphlabs.git  
  
2 Перейти в репозиторий  
cd graphlabs  

3 Установить docker и docker-compose  

4 Убедиться, что порты 5432, 5000, 5001, 5050 свободны  

5 Выполнить команду  
docker-compose up --build  
Это может занять некоторое время, т. к. необходимо скачать образы  

6 По адресу localhost:5050 можно работать с GraphLabs

